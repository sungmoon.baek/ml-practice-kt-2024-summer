# Machine Learning Course

* Google Classroom [link](https://classroom.google.com/c/NjkwNTQ3NzQzNDc2?cjc=wf723xk)

This repository contains Python code for a selection of tables, figures and LAB sections for 2024 KT-SNU AI education.
Many of the examples are from the book <A target="_blank" href='https://www.statlearning.com/'>'An Introduction to Statistical Learning with Applications in R'</A> by James, Witten, Hastie, Tibshirani (2013).<P>

<IMG src='Images/ISL_Cover_2.jpg' height=20% width=20%> <P>


#### References:
James, G., Witten, D., Hastie, T., Tibshirani, R. (2013). <I>An Introduction to Statistical Learning with Applications in  R</I>,  Springer Science+Business Media, New York.
https://www.statlearning.com/

Hastie, T., Tibshirani, R., Friedman, J. (2009). <I>Elements of Statistical Learning</I>, Second Edition, Springer Science+Business Media, New York.
https://web.stanford.edu/~hastie/ElemStatLearn/

